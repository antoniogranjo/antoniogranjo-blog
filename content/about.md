---
title: "About"
date: 2019-12-06T21:15:16+01:00
draft: false
layout: "about"
---
Mi nombre es Antonio Granjo.

Mi trabajo es poner software en producción. Mi puesto en mi empresa es "Software Architect", pero también soy "DevOps Coach".
